# mmecharts.js
 echarts3.x 数据、样式配置分离，jquery组件

1、同一份数据用在不同图表类型
2、自动解析不同数据结构
3、只修改需要修改的配置 


##目录
* [概述](#概述)
* [Options](#Options)
* [事件](#事件)
* [mmecharts方法](#mmecharts方法)
* [ChartType方法](#ChartType方法)
* [例子](#例子)

概述
-----------
 echarts3.x 数据、样式配置分离，jquery组件

1、同一份数据用在不同图表类型
2、自动解析不同数据结构
3、只修改需要修改的配置 

用法
-----------
导入`jquery.js`和`mm.echarts.js`和`echarts.js`
```javascript
$(function () {
 $(selector).mmEcharts(options); 
 }); 
 ```

Options
-----------

| 名称 	| 标签 	| 类型 | 	默认| 描述| 
--------- | -------- | -------- | --------| --------|
| chartType | data-chart-type | string |	line | 	图表类型;line:折线图,bar:柱状图,bar2:x轴和y轴调换的柱状图,pie:饼状图,map:地图
| queryUrl | 	data-query-url | 	string| 	undefined| 	请求后台数据URL|
| queryParams | 	data-query-params |	object| 	undefined |	请求后台参数|
| ajax | 	不支持 |	object |	undefined |	ajax请求配置|
| dataType | 	data-data-type |	string| 	row| 	请求后台返回数据类型，row或者column|
| chartTheme | 	data-chart-theme| 	string| 	walden |	图表主题|
| chartOptions|  	data-chart-options |	object |	undefined |	图表配置，可以只设置修改部分，会和默认配置合并|
| groupField | 	data-group-field |	string| 	name |	数据解码，分组字段|
| valueFields | 	data-value-fields |	object 	|undefined| 	数据解码，值字段,如,显示两条线:valueFields:{'dayValue':'日活完成量','monthTarget':'日活目标按月分解'}|
| categoryField | 	data-category-field |	string |	name |	类目字段，从行数据中获取，则需要配置该字段，如果设置categoryField则不需要设置valueFields|
| categoryValueField | 	data-category-value-field| 	string |	name| 	如果出现categoryField字段，必须要有categoryValueField字段
| dataset | 	不支持 | 	object | 	undefined | 	设置图表数据| 

事件
-----------
| 名称 	| 标签 	| 参数 | 描述| 
--------- | -------- | -------- | --------|
| on 	| 不支持 	| 	事件配置,{on:{click:function(){},mouserover:function(){}；另外还支持事件：before：配置开始;renderBefore：后台数据已经返回开始渲染;complete:渲染完成| 
| before | 	data-before | 	mmchart | 	配置开始| 
| renderBefore | 	data-render-before 	| data, params, mmchart |	后台数据已经返回开始渲染| 
| complete | 	data-complete | 	mmchart | 	渲染完成| 

mmecharts方法
-----------
| 名称 	| 参数 | 描述| 
--------- |-------- | --------|
|loadDataByUrl| 	url, params, dataType, defaultOptions, callback |	通过URL加装数据|
|addChartBySql| 	options, chartType, yAixs, sql, params, dataType, callback 	|添加混合图形|
|addChartByData 	options, chartType, yAixs, data, dataType, callback| 	添加混合图形|
|echarts| 	none |	获得echarts对象|
|setOption| 	options |	设置图表配置|
|getOption |	none |	获取图表配置|

ChartType方法
-----------
| 名称 	| 参数 | 描述| 
--------- |-------- | --------|
|type |	属性 |	图表类型|
|option |	defaultOption |	图表默认配置|
|decodeData |	data,mmecharts |	解码数据|

例子
-----------
[图表测试](http://htmlpreview.github.io/?https://github.com/parky18/mmecharts/blob/master/examples/all.html)  
[折线图测试](http://htmlpreview.github.io/?https://github.com/parky18/mmecharts/blob/master/examples/line.html)  
[柱状图测试](http://htmlpreview.github.io/?https://github.com/parky18/mmecharts/blob/master/examples/bar.html)  
[饼状图测试](http://htmlpreview.github.io/?https://github.com/parky18/mmecharts/blob/master/examples/pie.html)  
